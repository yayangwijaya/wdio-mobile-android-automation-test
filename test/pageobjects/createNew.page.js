class createNew {

    get title () {
        return $('id:title');
    };

    get subtask () {
        return $('id:task_edit_subtasks');
    };

    get startdate () {
        return $('id:task_edit_start_date');
    };

    get duedatebutton () {
        return $('id:due_date_button');
    };

    get okbutton () {
        return $('id:ok_button');
    };
    
    get saveicon() {
        return $('//android.widget.ImageButton[@content-desc="Save"]');
    };

    async createNewTask () {
        await this.title.waitForDisplayed();
        await this.title.setValue('Test Bank Mas - Yayang');
        await this.startdate.click();
        await this.duedatebutton.waitForDisplayed();
        await this.duedatebutton.click();
        await this.okbutton.click();
        await this.saveicon.click();
    };
};
module.exports = new createNew();