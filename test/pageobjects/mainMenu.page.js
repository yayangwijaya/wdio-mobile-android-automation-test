class mainMenu {
    /**
     * define selectors using getter methods
     */
    get add () {
        return $('id:fab');
    }

    get title () {
        return $ ('id:title');
    }

    async addData () {
        await this.add.waitForDisplayed();
        await this.add.click('Test Bank Mas - Yayang');
        
    };
}

module.exports = new mainMenu();
