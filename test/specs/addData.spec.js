const mainMenu = require('../pageobjects/mainMenu.page');
const createNew = require('../pageobjects/createNew.page');

describe('Create Task', () => {
    it('Create task', async () => {
        await mainMenu.addData();
        await createNew.createNewTask();
        await expect(mainMenu.title).toHaveText('Test Bank Mas - Yayang');
    });
});